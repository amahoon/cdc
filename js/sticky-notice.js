/**
 * File contains functionality to make a notice that sticks to the top of the page
 */

// make the notice sticky so that it's seen even when the page is scrolled
function stickyNotice() {
  // get the decor at the top of the introduction page
  var decor = $('.introduction > .decor');

  // listen for when the user is scrolling
  window.addEventListener('scroll', function(e) {
    // set our top point to be the height of the decor div
    var top = 0 - decor.height();

    // get the window's scroll top position
    var scrollTop = $(window).scrollTop();
    // get the decor's offset from our top point
    var elementOffset = decor.offset().top;
    // get how far from the our top point the decor is
    var distance = (elementOffset - scrollTop);

    // get the notice element
    var notice = $('.notice');
    // get the element that is going to make up for the notice height
    var cover_height = $('.cover-height');
    // set the height of the cover element to the notice height
    // once our notice is set to fixed, it's going to lose it's height and disjoint the positioning below it
    cover_height.css('height', notice.height() + 'px');

    // this is the class that will be added to make the notice stick
    var classname = 'fixed';
    // if the notice element exists on the page
    if (notice.length > 0) {
      // if the notice has reached our top point, make the notice sticky
      if (distance <= top) {
        if (!notice.hasClass(classname)) {
          notice.addClass(classname);
          // if the cover element is still hidden, make it visible
          if (cover_height.hasClass('hidden')) {
            cover_height.removeClass('hidden');
          }
        }
      }
      // if the notice is scrolled above our top point, remove the sticky notice
      else {
        if (notice.hasClass(classname)) {
          notice.removeClass(classname);
        }
        // if the cover element is still visible, make it hidden
        if (!cover_height.hasClass('hidden')) {
          cover_height.addClass('hidden');
        }
      }
    }
  });
}

$(document).ready(function() {
  // get the decor at the top of the introduction page
  var decor = $('.introduction > .decor');
  // if the decor exists on the page
  if (decor.length > 0) {
    stickyNotice();
  }
});