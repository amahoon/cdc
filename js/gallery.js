/**
 * File contains gallery functionality
 */

var window_width = $(window).width();
var scroll_pos = $(window).scrollTop();

// open the details for the photo in the gallery after clicking on the thumbnail
function openPhotoDetails(key, id) {
  scroll_pos = $(window).scrollTop();
  $('html').scrollTop(scroll_pos);

  var container = $('#' + id + ' .image');

  // url to get the photo details
  var url = 'includes/getPhotoDetails.php';
  $.ajax({
    type: 'POST',
    url: url,
    dataType: 'json',
    data: [],
    success: function(data) {
      // populate the details
      $('section.photo-details div.photo').html('<img src="' + container.attr('data-image-src') + '" alt="' + container.attr('data-image-alt') + '">');
      $('section.photo-details div.detail-container div.details p.title').html(data[key].title);
      $('section.photo-details div.detail-container div.details p.desc').html(data[key].description);
    },
    complete: function(data) {
      centerPhotoDetailsText();

      // mark the details as open
      $('section.photo-details').addClass('open');
      setTimeout(function() {
        $('body').addClass('open-details');
      }, 500);
    }
  });
}

function centerPhotoDetailsText() {
  window_width = $(window).width();

  var details_container = $('section.photo-details div.detail-container');
  var details = details_container.find('div.details');
  var close_button = details_container.find('div.close');
  details_container.scrollTop(0);

  $('section.photo-details div.detail-container div.details').attr('style', '');
  $('section.photo-details div.detail-container div.close').attr('style', '');

  var top_diff = (Math.round(details_container.outerHeight()) - Math.round(details.outerHeight())) / 2;
  if (top_diff > 0) {
    details.css('margin-top', top_diff);
    if (window_width < 896) {
      close_button.css('top', top_diff);
    }
  }

  window.onpopstate = function() {
    closePhotoDetails();
  };
  history.pushState({}, '');
}

function closePhotoDetails() {
  $('section.photo-details div.detail-container div.details').attr('style', '');
  $('section.photo-details').removeClass('open');
  $('body').removeClass('open-details');

  $('html').scrollTop(scroll_pos);
}

function sizeGallery() {
  var thumbnails = $('section.gallery .thumbnail');
  if (thumbnails.length > 0) {
    var classname = 'col-';
    var full_width = 12;
    // $narrow-screen in scss
    var new_class = (window_width > 608) ? (classname + (full_width / 3)) : (classname + (full_width / 2));

    // loop through the thumbnails
    thumbnails.each(function() {
      // loop through the possible col sizes to remove the "col-" classes
      for (var i = 1; i < full_width+1; i++) {
        // if the "col-" class exists, remove it
        if ($(this).hasClass(classname + i)) {
          $(this).removeClass(classname + i);
        }
      }
      // add the new "col-" class to the thumbnail
      $(this).addClass(new_class);

      $(this).click(function(e) {
        // console.log(e);
        var content = $(this).find('.entry-content');
        // e.preventDefault();
        // console.log(content.attr('id'));
        // console.log(content.attr('data-photo-id'));
        openPhotoDetails(content.attr('data-photo-id'), content.attr('id'));
      });
    });

    // get the width of each gallery thumbnail
    var width = $('section.gallery .thumbnail').width();
    // set the height to defautl to the width
    var height = width;

    // get the largest number for the viewport width and height
    var vw = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    var vh = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    // fi the width is greater than 608, break the height into 3
    if (vw > 896) {
      height = vh / 3;
    }
    // set each column width to the defined height
    // note that we round up with ceil because using decimal places causes jumpy effects for the image hover effects
    var cw = $('section.gallery .column').height(Math.ceil(height));

    // loop through the thumbnails in the gallery
    $('section.gallery .column div.entry-content').each(function() {
      // get the image element (hold the alt and src for the details)
      var image = $(this).find('.background div.image');
      // add the image as a background image in the css
      image.css('background-image', 'url("' + image.attr('data-image-src') + '")');

      // get the title container and the title
      var title_container = $(this).find('.hover-title');
      // var title = $(this).find('.hover-title p.title');
      var title = $(this).find('.hover-title p.title span.text');
      // get the image alt for the title
      title.html('<p>' + image.attr('data-image-alt') + '</p>');

      // get the offset inside each thumbnail to find the center and set it to the title
      var offset = (title_container.outerHeight() - title.outerHeight()) / 2;
      title.css('margin-top', offset);
    });
  }
}

$(document).ready(function() {
  sizeGallery();
});
$(window).resize(function() {
  sizeGallery();
  if ($('section.photo-details').hasClass('open')) {
    window_width = $(window).width();
    if (window_width > 896) {
      centerPhotoDetailsText();
    }
    else {
      $('section.photo-details div.detail-container div.details').attr('style', '');
      $('section.photo-details div.detail-container div.close').attr('style', '');
    }
  }
});