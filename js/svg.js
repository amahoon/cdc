/*
 * Replace all SVG images with inline SVG
 */
jQuery('img.svg').each(function(){
  var $img = jQuery(this);
  var imgID = $img.attr('id');
  var imgClass = $img.attr('class');
  var imgURL = $img.attr('src');

  jQuery.get(imgURL, function(data) {
    // Get the SVG tag, ignore the rest
    var $svg = jQuery(data).find('svg');

    // Add replaced image's ID to the new SVG
    if (typeof imgID !== 'undefined') {
      $svg = $svg.attr('id', imgID);
    }
    // Add replaced image's classes to the new SVG
    if (typeof imgClass !== 'undefined') {
      $svg = $svg.attr('class', imgClass+' replaced-svg');
    }

    // Remove any invalid XML tags as per http://validator.w3.org
    $svg = $svg.removeAttr('xmlns:a');

    var height = $svg.attr('height'),
        width = $svg.attr('width');

    // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
    if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
      $svg.attr('viewBox', '0 0 ' + height.replace('px', '') + ' ' + width.replace('px', ''));
    }

    var id = $svg.attr('id'),
        classes = $svg.attr('class');
    if (id !== undefined) {
      // footer logo
      if (id == 'mahonlogo') {
        $svg.attr('viewBox', '0 0 ' + (height.replace('px', '') * 1.5) + ' ' + (width.replace('px', '') * 1.5));
        $svg.attr('enable-background', 'new ' + $svg.attr('viewBox'));
        $svg.attr('width', (width.replace('px', '') * 1.5) + 'px');
        $svg.attr('height', (height.replace('px', '') * 1.5) + 'px');
      }
    }
    if (classes !== undefined) {
      if (classes.includes('{ class }')) {
        // $svg.attr('width', (width.replace('px', '') / 1.5) + 'px');
        // $svg.attr('height', (height.replace('px', '') / 1.5) + 'px');
      }
    }

    // Replace image with new SVG
    $img.replaceWith($svg);

  }, 'xml');

});