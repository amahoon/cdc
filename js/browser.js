var init_val = $(window).scrollTop();

var window_width = $(window).width();
var window_height = $(window).height();

var ipad = ((/iPad/i.test(navigator.userAgent)) === true) ? true : false;
var browser_device = ((/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) === true) ? 'Mobile Device' : 'Browser';

function showWindowMeasurements() {
	window_width = $(window).width();
	window_height = $(window).height();

	var window_measurements = $('.window-measurements');
	if (window_measurements.length > 0) {
		window_measurements.each(function() {
			$(this).find('.width').html('<strong>Width:</strong> ' + window_width);
			$(this).find('.height').html('<strong>Height:</strong> ' + window_height);
			$(this).find('.device').html('<strong>Device:</strong> ' + ipad);
		});	
	}
}

// is mobile or browser and add classes accordingly
function setDeviceStatus() {
	window_width = $(window).width();

	if (browser_device == 'Browser') {
		// $small-screen in scss
		if (window_width > 464) {
			if ($('body').hasClass('mobile')) {
				$('body').removeClass('mobile');
			}
		}
		else {
			$('body').addClass('mobile');
		}
	}
	else {
		$('body').addClass('mobile-device');
	}
}

$(document).ready(function() {
  window_width = $(window).width();
  showWindowMeasurements();
  setDeviceStatus();
});
$(window).resize(function() {
  window_width = $(window).width();
  showWindowMeasurements();
  setDeviceStatus();
});