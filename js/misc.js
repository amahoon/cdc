/**
 * File contains miscellaneous functionality
 */

var window_width = $(window).width();

function sizeQuote() {
  // get all quote holders on the page
  var quote_holders = $('.quote-holder');
  // loop through our quote containers
  quote_holders.each(function() {
    // get the contents of the quote
    var quote = $(this).find('.quote .content');
    // clear the holders style attribute (in case we are resizing and the height has been set before)
    $(this).attr('style', '');
    // get the height of this element (padding, margin, border)
    var height = $(this).height();
    // add the height of the element to the height of the quote +5px (it looks slightly off without the extra pixels)
    height = height + quote.height() + 5;
    // round the height to 1 decimal place
    $(this).height(Math.round(height));
  });
}

function positionLetterLogo() {
  // if one the covid-19 letter page
  var letter = $('section.covid19-letter');
  if (letter.length > 0) {
    // get all quote holders on the page
    var quote_holders = $('.quote-holder');
    // loop through our quote containers
    quote_holders.each(function() {
      // get the logo
      var logo = $(this).parent().find('.logo-container .logo');
      // get the difference between the quote height and the logo height and then divide it by 2 to get the amount to move the logo down
      var top_diff = (Math.round($(this).outerHeight()) - Math.round(logo.outerHeight())) / 2;
      // move the logo down so it's centered with the quote
      logo.css('top', top_diff);
    });
  }
}

$(document).ready(function() {
  window_width = $(window).width();
  sizeGallery();
  // $smallest-screen in scss
  if (window_width > 415) {
    positionLetterLogo();
  }
});
$(window).resize(function() {
  window_width = $(window).width();
  sizeGallery();
  // $smallest-screen in scss
  if (window_width > 415) {
    positionLetterLogo();
  }
});