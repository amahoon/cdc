<?php
  // SITE BASE URL
  $base_url = "$_SERVER[DOCUMENT_ROOT]$_SERVER[REQUEST_URI]";
  // $base_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  // URL TO OUR INCLUDE FILES
  $views_url = 'includes/views/';

  // DEFAULT CONFIG FILE
  include 'includes/config.php';

  error_reporting( E_ALL );
?>
<!doctype html>
<html lang="en" dir="ltr">
<head>
  <!-- FAVICONS -->
  <?php include 'includes/favicons.php'; ?>
  <!-- METATAGS -->
  <?php include 'includes/metatags.php'; ?>
  <!-- FONTS -->
  <?php include 'includes/fonts.php'; ?>
  <!-- CSS -->
  <?php include 'includes/css.php'; ?>
</head>
<body>
  <!-- DEVELOPMENT -->
  <?php // include $views_url . 'dev/window-measurements.php'; ?>
  <?php // include $views_url . 'dev/breakpoints.php'; ?>

  <!-- HEADER -->
  <?php include $views_url . 'header.php'; ?>

  <!-- SECTIONS -->
  <?php include $views_url . 'sections/introduction.php'; ?>
  <?php include $views_url . 'sections/gallery.php'; ?>

  <!-- FOOTER -->
  <div class="mobile-footer">
    <?php include $views_url . 'footer.php'; ?>
  </div>

  <!-- JAVASCRIPT -->
  <?php include 'includes/scripts.php'; ?>
</body>
</html>