var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var plumber = require ('gulp-plumber');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var del = require('del');

var jsSources = ['js/**/*.js'];
var sassSources = ['sass/**/*.scss'];
var outputDir = 'dist/';

// report errors but continue gulpin'
var onError = function (err) {
  gutil.beep();
  console.log(err.messageFormatted);
  this.emit('end'); // super crit
};

// Remove the compiled css and js files
function clean() {
  return del([ 'dist/css', 'dist/js' ]);
}

// Compile scss into css files to style site output
var sassBuild = function() {
  return gulp.src(sassSources)
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(outputDir + 'css'));
};

// Rename, minify, and move Javascript files
var jsBuild = function() {
  return gulp.src(jsSources)
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(rename(function (path) {
      path.basename += ".min";
      path.extname = ".js";
    }))
    .pipe(uglify())
    .pipe(gulp.dest(outputDir + 'js'));
};

// Watch function for the different tasks that are likely to change
function watch() {
  gulp.watch(jsSources[0], jsBuild);
  gulp.watch(sassSources[0], sassBuild);
}
 
// Specify if tasks run in series or parallel using `gulp.series` and `gulp.parallel`
var build = gulp.series(clean, gulp.parallel(sassBuild, jsBuild));
 
// Declare Tasks
exports.clean = clean;
exports.styles = sassBuild;
exports.scripts = jsBuild;
exports.watch = watch;
exports.build = build;
// Define default task that can be called by just running `gulp` from cli
exports.default = build;

