<?php
  $dist = 'dist/js/';
  $vendor = $dist . 'vendor/';
?>

<!-- node_modules -->
  <!-- jQuery -->
    <!-- allow jQuery on the site -->
    <script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
    <!-- add the ability to make like tags match their container heights -->
    <script type="text/javascript" src="node_modules/jquery-match-height/jquery.matchHeight.js"></script>

<!-- vendor -->
  <!-- jQuery -->
    <!-- align elements vertically in a container -->
    <script defer type="text/javascript" src="<?=$vendor?>jQuery-Flex-Vertical-Center-master/jquery.flexverticalcenter.min.js"></script>

<!-- external -->

<!-- custom javascript -->
  <!-- responsive tasks : ex. mobile menu -->
  <script type="text/javascript" src="<?=$dist?>browser.min.js"></script>
  <!-- gallery functions -->
  <script type="text/javascript" src="<?=$dist?>gallery.min.js"></script>
  <!-- miscellanous javascript functions -->
  <script type="text/javascript" src="<?=$dist?>misc.min.js"></script>
  <!-- sticky notice to the top of the page -->
  <script type="text/javascript" src="<?=$dist?>sticky-notice.min.js"></script>
  <!-- functionality to turn img tags with svg sources into svg tags and update sizing -->
  <script type="text/javascript" src="<?=$dist?>svg.min.js"></script>

<!-- OTHER JAVASCRIPT -->
<script>
	// HIDE LOADER WHEN PAGE IS FINISHED LOADING
	// window.onload = function() {
	// 	$('body').addClass('loaded');
	// 	window.scrollTo(0, 0);
	// }
</script>