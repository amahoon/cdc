<?php include 'tags.php'; ?>

<meta charset="utf-8">
<meta http-equiv="content-language" content="en">
<meta name="referrer" content="no-referrer">
<meta name="MobileOptimized" content="width">
<meta name="HandheldFriendly" content="true">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $tags['title']; ?></title>

<meta property="og:title" content="<?php echo $tags['title']; ?>">
<meta property="og:type" content="website">
<meta property="og:image" content="<?php echo $tags['image']; ?>">
<meta property="og:url" content="<?php echo $tags['url']; ?>">
<meta property="og:description" content="<?php echo $tags['desc']; ?>">

<meta name="twitter:title" content="<?php echo $tags['title']; ?>">
<meta name="twitter:url" content="<?php echo $tags['url']; ?>">
<meta name="twitter:description" content="<?php echo $tags['desc']; ?>">
<meta name="twitter:image" content="<?php echo $tags['image']; ?>">
<!-- <meta name="" content=""> -->

<meta name="description" content="<?php echo $tags['desc']; ?>">
<meta name="keywords" content="<?php echo $tags['keywords']; ?>">

<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="expires" content="0" />
<!-- <meta http-equiv="expires" content="Wed, 31 Dec 2025 23:59:59 ET"> -->
<!-- <meta http-equiv="pragma" content="no-cache" /> -->
