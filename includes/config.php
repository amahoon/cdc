<?php 
  // SITE BASE URL
  $base_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  // $base_url = "$_SERVER[DOCUMENT_ROOT]$_SERVER[REQUEST_URI]";
  
  // an array of our includes files
  $includes = array();
  // if we are using our local environment
  if (strpos($base_url,'localhost:8888') !== 0) {
    // set the root of our include declarations to our includes files
    $root = 'includes/';
    // include our Debugger object which has functions to help us debug our code
    $includes[] = $root . 'helpers/Debug.php';
    // the folder name in our htdocs
    $folder = '__template';
    // override the base url for our dev environment
    $base_url = '/Applications/MAMP/htdocs/' . $folder . '/';
  } else {
    // set the root to an empty string when we aren't on a dev environment
    $root = '';
  }

  // include our Connection object which provides a connection to our database
  $includes[] = $root . 'helpers/Connection.php';

  // include any files that we have added to our $includes array that aren't already added
	foreach ($includes as $file) {
    // check if the file is already included
  	if (!in_array($base_url . $file,get_included_files())) {
      // include the file using our base_url
      include $base_url . $file;
    }
	}

  // DEBUG EXAMPLE
  // $debug = new Debug();
  // $debug->dpm($base_url);