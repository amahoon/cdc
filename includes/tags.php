<?php

	$tags['name'] = 'Choice Directed Care';
  $tags['abbrev'] = 'CDC';
	$tags['slogan'] = '{ site slogan }';
    // used as title field     &#124; = |
    $tags['title'] = $tags['abbrev'] . ' &#124; ' . $tags['name']; // TEMP | Template Site
    // $tags['title'] = $tags['name'] . ' &#124; ' . $tags['slogan']; // Template Site | Template Slogan
	$tags['desc'] = 'Introducing a new and innovative leadership team with over fifteen years collectively supporting persons and families to enable best life experiences!';
	$tags['keywords'] = ''; // space separated
	$tags['author'] = 'Alyssa Mahon'; // creator of the site
	$tags['url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$tags['image'] = $tags['url'] . '/images/logos/logo.png';