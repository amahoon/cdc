<?php

$details = [
  [
    'title' => 'Lifelong Wishes Fulfilled',
    'description' => '<span>Supporting a life long wish of attending the largest North American Pow Wow in Albuquerque, New Mexico for two years in a row – and still going! Support includes assisting with a yearly budget plan and support from the planning stages to attending and home again.</span>'
  ],
  [
    'title' => 'Four wheeling',
    'description' => '<span>Adventure awaits on a four wheeling adventure tour in the Blue Mountains. Spring brought along allot of laughs and mud while navigating the hair pin turns along the trails.</span>'
  ],
  [
    'title' => 'Sports and Leisure',
    'description' => '<span>Physical activity in the pool at the Bracebridge Sportsplex. An opportunity to cool off and burn off energy while having fun at the same time!</span>'
  ],
  [
    'title' => 'Cuba Vista',
    'description' => '<span><p>Memories made from a poolside view just steps from the beach at our resort in Cuba. Being able to save and budget for a yearly trip empowers world travellers!</p></span>'
  ],
  [
    'title' => 'Our Services',
    'description' => '<span><p><span class="company-name">Choice Directed Care</span> offers services and support for:</p>
      <ul>
        <li><p>Wrap around Person Directed planning and One Page Profiles</p></li>
        <li><p>Passport funding (empowerment through self-management)</p></li>
        <li><p>Respite funding (youth and adult)</p></li>
        <li><p>S.S.A.H. (Special Services at Home)</p></li>
        <li><p>Recreational and Social learning opportunities</p></li>
        <li><p>Day-to-Day Support including medical, dental and specialist appointments</p></li>
        <li><p>Support and transportation to special events in and out of town</p></li>
        <li><p>Assistance with grocery and personal shopping</p></li>
        <li><p>Educational learning opportunities including nutrition and meal preparation, personal care, budgeting and more</p></li>
        <li><p>Resume building and employment services</p></li>
        <li><p>Community partnering with agencies and service providers</p></li>
        <li><p>Navigating through the Judicial System</p></li>
        <li><p>Getting out for a walk or having a talk</p></li>
      </ul>
    </span>'
  ],
  [
    'title' => 'Niagara Falls',
    'description' => '<span>Lots of fun with friends on the Hornblower Cruise in Niagara Falls!</span>'
  ],
  [
    'title' => 'The Ottawa War Museum',
    'description' => '<span>Adventures await and history relived during our visits to the Ottawa War Museum and Deifenbunker!</span>'
  ],
  [
    'title' => 'Crafts',
    'description' => '<span>Look at the beautiful resin Charcuterie Board that was completed in just under 5 hours! This is just one of many projects that CDC makes possible for everyone!</span>'
  ],
  [
    'title' => 'Pottery Classes',
    'description' => '<span>Channeling our creative minds with both five-week and specialty classes with Tracy at Northwinds Pottery here in Parry Sound. The feeling of creating works of art to display or giveaway is always a treat.</span>'
  ],
];

$json = json_encode($details);
echo $json;