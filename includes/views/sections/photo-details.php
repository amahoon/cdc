<section class="photo-details close">
  <div class="photo">
    <img src="/" alt="stock photo">
  </div>
  <div class="detail-container">
    <div class="close" onclick="closePhotoDetails()">X</div>
    <div class="details">
      <p class="title">This is the Photo's Title</p>
      <?php include 'includes/views/decor.php' ?>
      <p class="desc"><span>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.Morbi accumsan ipsum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</span><br><span>Mauris in erat justo. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</span></p>
    </div>
  </div>
</section>