<?php $home = '/'; ?>
<section class="covid19-letter">
    <?php include 'includes/views/decor.php' ?>

    <div class="container row col-12 header">
    	<div class="return top">
    		<a href="<?php echo $home ?>">Home</a>
    	</div>
    	<div class="logo-container">
		    <div class="logo">
		    	<a href="<?php echo $home ?>">
		    		<img class="full-logo" src="images/logos/CDC-logo.png" alt="Choice Directed Care (CDC) logo">
			      <img class="logo-only" src="images/logos/CDC-logo-only.png" alt="Choice Directed Care (CDC) logo">
			      <img class="name-only" src="images/logos/CDC-name-only.png" alt="Choice Directed Care (CDC)">
			    </a>
		    </div>
		  </div>
	    <div class="quote-holder">
		    <div class="quote">
		    	<div class="content center">
		    		<p>Remember we are all in this together and together, we can stay strong and safe!</p>
		    	</div>
		    </div>
		  </div>
	  </div>

    <div class="container row col-12">
    	<div class="letter">
	      <p class="identification">To Our Supported Families,</p>
	      <p>As part of our care and commitment to the families and persons we support under <span class="company-name">Choice Directed Care</span>, we have taken the following steps:</p>
	      <p>In case of an emergency, we are asking that each person follow the protocol set in place with our Service Agency to call an ambulance or a counsellor should you require immediate medical/emotional support.</p>
	      <p>The care of the people we support and our staff is of high importance and we want to ensure that we are all safe. We will continue to liaise with our community partners to ensure all paperwork and needs are being met and processed on a timely basis. We encourage our <span class="company-name">Choice Directed Care</span> family to reach out if you want to talk or "share a coffee or tea" via phone or FaceTime.</p>
	      <p>We will continue to check in daily and remind everyone of the following:</p>
	      <ul>
	      	<li><p>Wash hands in warm water for <span class="emphasis">at least 20 seconds</span> or to the song "Happy Birthday". Do this <span class="emphasis">each time</span> you are out in the community, when using the facilities or preparing food.</p></li>
	      	<li><p>Please try not to touch your nose, mouth or eyes.</p></li>
	      	<li><p>Avoid close contact with people who are sick and practice social (physical) distancing.</p></li>
	      	<li><p>Cough and sneeze into your sleeve and <span class="emphasis">never</span> into your hands.</p></li>
	      	<li><p>Practice social distancing, about two arms lengths away (approximately 2 metres).</p></li>
	      	<li><p>Use approved cleaning solutions / wipes to clean surfaces that have come into contact with another person or object. If you don't have anti-bacterial wipes, wash objects in <span class="emphasis">warm water with soap.</span></p></li>
	      	<li><p>If you have a cough, please self-isolate. That means keeping away from people and disinfecting your home.</p></li>
	      	<li><p>Avoid visits with older adults, or those with medical conditions.</p></li>
	      	<li><p>Use the wipes / hand-sanitizer stations located at all community locations when you walk into public places and when you leave.</p></li>
	      	<li><p><span class="emphasis">If you suspect that you have symptoms,</span> please connect with your family doctor or public health immediately and follow their advice. Symptoms include a fever greater than 38˚C, a cough and difficulty breathing.</p></li>
	      </ul>
	      <p class="reminder">Remember we are all in this together and together, we can stay strong and safe!</p>
	    </div>
	    <div class="for-more-information">
	    	<h2>For More Information</h2>
	    	<div class="links">
		    	<ul>
		    		<li><a href="https://www.ontario.ca/page/reopening-ontario-after-covid-19" target="_blank">Reopening Ontario after COVID-19</a></li>
		    		<li><a href="https://covid-19.ontario.ca/" target="_blank">Ontario Updates for COVID-19</a></li>
		    		<li><a href="https://www.canada.ca/" target="_blank">Government of Canada Website</a></li>
		    		<li><a href="https://www.canada.ca/coronavirus" target="_blank">Canadian Information Regarding COVID-19</a></li>
		    	</ul>
		    </div>
	    	<p>For any further questions, please call <a href="tel:1-833-784-4397">1-833-784-4397</a> or visit us online at <a href="https://www.canada.ca/coronavirus" target="_blank">canada.ca/coronavirus</a>.</p>
	    </div>
    </div>

    <div class="container row col-12">
    	<div class="return bottom">
    		<a href="<?php echo $home ?>">Home</a>
    	</div>
    </div>

    <?php include 'includes/views/footer.php' ?>
  </section>