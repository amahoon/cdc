<section class="gallery">
  <h2 class="hidden">Gallery</h2>
  <div class="row col-12">
    <div class="grid thumbnail column col-4">
      <div class="entry-content" id="lifelong-wishes-fulfilled" data-photo-id="0">
        <div class="hover-title">
          <p class="title"><span class="text"></span></p>
        </div>
        <div class="background">
          <div class="image block" data-image-src="images/gallery/lifelong-wishes-fulfilled.jpg" data-image-alt="Lifelong Wishes Fulfilled"></div>
        </div>
      </div>
    </div>
    <div class="grid thumbnail column col-4">
      <div class="entry-content" id="four-wheeling"data-photo-id="1">
        <div class="hover-title">
          <p class="title"><span class="text"></span></p>
        </div>
        <div class="background">
          <div class="image block" data-image-src="images/gallery/four-wheeling.jpg" data-image-alt="Four Wheeling"></div>
        </div>
      </div>
    </div>
    <div class="grid thumbnail column col-4">
      <div class="entry-content" id="sports-and-leisure" data-photo-id="2">
        <div class="hover-title">
          <p class="title"><span class="text"></span></p>
        </div>
        <div class="background">
          <div class="image block" data-image-src="images/gallery/sports-and-leisure.jpg" data-image-alt="Sports and Leisure"></div>
        </div>
      </div>
    </div>
    <div class="grid thumbnail column col-4">
      <div class="entry-content" id="cuba-vista" data-photo-id="3">
        <div class="hover-title">
          <p class="title"><span class="text"></span></p>
        </div>
        <div class="background">
          <div class="image block" data-image-src="images/gallery/cuba-vista.jpg" data-image-alt="Cuba Vista"></div>
        </div>
      </div>
    </div>
    <div class="grid thumbnail column col-4">
      <div class="entry-content" id="our-services" data-photo-id="4">
        <div class="hover-title">
          <p class="title"><span class="text"></span></p>
        </div>
        <div class="background">
          <div class="image block" data-image-src="images/gallery/sandra-linda.jpg" data-image-alt="Our Services"></div>
        </div>
      </div>
    </div>
    <div class="grid thumbnail column col-4">
      <div class="entry-content" id="niagara-falls" data-photo-id="5">
        <div class="hover-title">
          <p class="title"><span class="text"></span></p>
        </div>
        <div class="background">
          <div class="image block" data-image-src="images/gallery/niagara-falls.jpg" data-image-alt="Niagara Falls"></div>
        </div>
      </div>
    </div>
    <div class="grid thumbnail column col-4">
      <div class="entry-content" id="ottawa-war-museum" data-photo-id="6">
        <div class="hover-title">
          <p class="title"><span class="text"></span></p>
        </div>
        <div class="background">
          <div class="image block" data-image-src="images/gallery/ottawa-war-museum.jpg" data-image-alt="The Ottawa War Museum"></div>
        </div>
      </div>
    </div>
    <div class="grid thumbnail column col-4">
      <div class="entry-content" id="crafts" data-photo-id="7">
        <div class="hover-title">
          <p class="title"><span class="text"></span></p>
        </div>
        <div class="background">
          <div class="image block" data-image-src="images/gallery/crafts.jpg" data-image-alt="Crafts"></div>
        </div>
      </div>
    </div>
    <div class="grid thumbnail column col-4">
      <div class="entry-content" id="pottery" data-photo-id="8">
        <div class="hover-title">
          <p class="title"><span class="text"></span></p>
        </div>
        <div class="background">
          <div class="image block" data-image-src="images/gallery/pottery.jpg" data-image-alt="Pottery Classes"></div>
        </div>
      </div>
    </div>
    <div class="grid thumbnail non-interactive column col-4">
      <div class="entry-content" id="logo">
        <div class="background">
          <div class="image block" data-image-src="images/logos/CDC-logo-only.png" data-image-alt="Choice Directed Care (CDC) logo"></div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'photo-details.php'; ?>