<section class="introduction">
  <?php include 'includes/views/decor.php' ?>
  <?php include 'includes/views/notice.php' ?>

  <div class="main-content container row col-12">
    <div class="logo">
      <img src="images/logos/CDC-logo.png" alt="Choice Directed Care (CDC) logo">
    </div>
    <div class="slogan">
      <p><span class="slogan-container"><span>Innovative</span><span>creative</span><span>supportive</span></span></p>
    </div>
    <div>
      <h2>Introducing</h2>
      <p class="introduction"><span>A new and innovative leadership team with over twenty years collectively supporting persons and families to enable best life experiences! <b>Choice Directed Care</b> is a new not-for-profit organization serving Parry Sound and District.<br></span><span class="tagline">A new choice... your choice.</span></p>
    </div>

    <div class="quote-holder container row col-12">
      <div class="quote center">
        <div class="content center">
          <p>Creating meaningful and purposeful lives through holistic practices in motivation, participation and celebration of life!</p>
        </div>
      </div>
    </div>

    <div class="contacts">
      <ul>
        <li>
          <p class="name">Linda <span>Kosanyi</span></p>
          <p class="email"><a href="mailto:linda@choicedirectedcare.com" class="email"><span>linda@choicedirectedcare.com</span></a></p>
          <p class="phone"><a href="tel:705-938-1607" class="phone"><span>(705) 938-1607</span></a></p>
        </li>
        <li>
          <p class="name">Sandra <span>Walters Ladan</span></p>
          <p class="email"><a href="mailto:sandra@choicedirectedcare.com" class="email"><span>sandra@choicedirectedcare.com</span></a></p>
          <p class="phone"><a href="tel:705-746-5409" class="phone"><span>(705) 746-5409</span></a></p>
        </li>
      </ul>
    </div>
  </div>

  <div class="desktop-footer">
    <div class="footer-cover-height"></div>
    <?php include 'includes/views/footer.php' ?>
  </div>
</section>