<?php

/**
 * @file
 * Creates an object that will help with debugging.
 */

class Debug {

  public function dpm($var) {
    echo '<div style="background-color: white;"><pre>' . var_export($var,TRUE) . '</pre></div>';
  }

}