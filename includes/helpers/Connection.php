<?php

/**
 * @file
 * Creates an object to connect to the database.
 */
	
class Connection {

	public $mysqli;

  public $fields;

  public $tables;

  public $conditions;

  public $order;

  public $range;

  public $group;

  public $escapedNames;

  // public $insert;

	public function __construct() {
		$this->mysqli = new mysqli("localhost", "root", "root", "rebekahandaaron_dev");
	}

	public function select($table, $alias = NULL) {
    return $this->addJoin(NULL,$table,$alias);
	}

  public function insert($table, $fields, $values) {
    $sql = 'INSERT INTO ' . $table . ' ( ' . implode($fields,', ') . ' ) VALUES ( ' . implode($values,', ') . ' )';
    $results = $this->mysqli->query($sql);

    return $this->mysqli->insert_id;
  }

  /**
   * {@inheritdoc}
   */
  public function fields($table_alias, array $fields = []) {
    if ($fields) {
      foreach ($fields as $field) {
        // We don't care what alias was assigned.
        $this->addField($table_alias, $field);
      }
    } else {
      // We want all fields from this table.
      $this->tables[$table_alias]['all_fields'] = TRUE;
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addField($table_alias, $field, $alias = NULL) {
    // If no alias is specified, first try the field name itself.
    if (empty($alias)) {
      $alias = $field;
    }

    // If that's already in use, try the table name and field name.
    if (!empty($this->fields[$alias])) {
      $alias = $table_alias . '_' . $field;
    }

    $this->fields[$alias] = [
      'field' => $field,
      'table' => $table_alias,
      'alias' => $alias,
    ];

    return $alias;
  }

  /**
   * {@inheritdoc}
   */
  public function join($table, $alias = NULL, $condition = NULL) {
    return $this->addJoin('INNER', $table, $alias, $condition);
  }

  /**
   * {@inheritdoc}
   */
  public function leftJoin($table, $alias = NULL, $condition = NULL) {
    return $this->addJoin('LEFT OUTER', $table, $alias, $condition);
  }

  /**
   * {@inheritdoc}
   */
  public function rightJoin($table, $alias = NULL, $condition = NULL) {
    return $this->addJoin('RIGHT OUTER', $table, $alias, $condition);
  }

  /**
   * {@inheritdoc}
   */
  public function addJoin($type, $table, $alias = NULL, $condition = NULL) {
    if (is_null($alias)) {
      $alias = $table;
    }

    $this->tables[$alias] = [
      'join type' => $type,
      'table' => $table,
      'alias' => $alias,
      'condition' => $condition,
    ];

    return $alias;
  }

  /**
   * {@inheritdoc}
   */
  public function condition($field, $value = NULL, $operator = '=') {
    if (empty($operator)) {
      $operator = '=';
    }

    $this->conditions[] = [
      'field' => $field,
      'value' => (is_numeric($value)) ? $value : '"' . $value . '"',
      'operator' => $operator,
    ];

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function orderBy($field, $direction = 'ASC') {
    // Only allow ASC and DESC, default to ASC.
    $direction = strtoupper($direction) == 'DESC' ? 'DESC' : 'ASC';
    $this->order[$field] = $direction;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function range($start = NULL, $length = NULL) {
    $this->range = $start !== NULL ? ['start' => $start, 'length' => $length] : [];
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function groupBy($field) {
    $this->group[$field] = $field;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    // SELECT
    $query = 'SELECT ';

    // FIELDS and EXPRESSIONS
    $fields = [];
    foreach ($this->tables as $alias => $table) {
      if (!empty($table['all_fields'])) {
        $fields[] = $this->escapeTable($alias) . '.*';
      }
    }
    foreach ($this->fields as $field) {
      // Always use the AS keyword for field aliases, as some
      // databases require it (e.g., PostgreSQL).
      $fields[] = (isset($field['table']) ? $this->escapeTable($field['table']) . '.' : '') . $this->escapeField($field['field']) . ' AS ' . $this->escapeAlias($field['alias']);
    }
    $query .= implode(', ', $fields);

    // FROM - We presume all queries have a FROM, as any query that doesn't won't need the query builder anyway.
    $query .= "\nFROM ";
    foreach ($this->tables as $table) {
      $query .= "\n";
      if (isset($table['join type'])) {
        $query .= $table['join type'] . ' JOIN ';
      }

      // If the table is a subquery, compile it and integrate it into this query.
      if ($table['table']) {
        $table_string = $this->escapeTable($table['table']);
      }
      $query .= $table_string . ' AS ' . $this->escapeTable($table['alias']);

      if (!empty($table['condition'])) {
        $query .= ' ON ' . (string) $table['condition'];
      }
    }

    // WHERE
    if (count($this->conditions)) {
      $i = 0;
      $length = count($this->conditions);
      foreach ($this->conditions as $where) {
        // There is an implicit string cast on $this->condition.
        if ($i == 0) {
          $query .= "\nWHERE " . $where['field'] . " " . $where['operator'] . " " . $where['value'];
        } else {
          $query .= "\nAND " . $where['field'] . " " . $where['operator'] . " " . $where['value'];
        }
        $i++;
      }
    }

    // // GROUP BY
    if ($this->group) {
      $query .= "\nGROUP BY " . implode(', ', $this->group);
    }

    // // ORDER BY
    if ($this->order) {
      $query .= "\nORDER BY ";
      $fields = [];
      foreach ($this->order as $field => $direction) {
        $fields[] = $this->escapeField($field) . ' ' . $direction;
      }
      $query .= implode(', ', $fields);
    }

    // RANGE
    // There is no universal SQL standard for handling range or limit clauses.
    // Fortunately, all core-supported databases use the same range syntax.
    // Databases that need a different syntax can override this method and
    // do whatever alternate logic they need to.
    if (!empty($this->range)) {
      $query .= "\nLIMIT " . (int) $this->range['length'] . " OFFSET " . (int) $this->range['start'];
    }
    
    return $query;
  }

  /**
   * Escapes a table name string.
   *
   * Force all table names to be strictly alphanumeric-plus-underscore.
   * For some database drivers, it may also wrap the table name in
   * database-specific escape characters.
   *
   * @param string $table
   *   An unsanitized table name.
   *
   * @return string
   *   The sanitized table name.
   */
  public function escapeTable($table) {
    if (!isset($this->escapedNames[$table])) {
      $this->escapedNames[$table] = preg_replace('/[^A-Za-z0-9_.]+/', '', $table);
    }
    return $this->escapedNames[$table];
  }

  /**
   * Escapes a field name string.
   *
   * Force all field names to be strictly alphanumeric-plus-underscore.
   * For some database drivers, it may also wrap the field name in
   * database-specific escape characters.
   *
   * @param string $field
   *   An unsanitized field name.
   *
   * @return string
   *   The sanitized field name.
   */
  public function escapeField($field) {
    if (!isset($this->escapedNames[$field])) {
      $this->escapedNames[$field] = preg_replace('/[^A-Za-z0-9_.]+/', '', $field);
    }
    return $this->escapedNames[$field];
  }

  /**
   * Escapes an alias name string.
   *
   * Force all alias names to be strictly alphanumeric-plus-underscore. In
   * contrast to DatabaseConnection::escapeField() /
   * DatabaseConnection::escapeTable(), this doesn't allow the period (".")
   * because that is not allowed in aliases.
   *
   * @param string $field
   *   An unsanitized alias name.
   *
   * @return string
   *   The sanitized alias name.
   */
  public function escapeAlias($field) {
    if (!isset($this->escapedAliases[$field])) {
      $this->escapedAliases[$field] = preg_replace('/[^A-Za-z0-9_]+/', '', $field);
    }
    return $this->escapedAliases[$field];
  }

  public function execute() {
    $sql = $this->__toString();
    $results = $this->mysqli->query($sql);
    return $results;
  }

  public function arrayResults($q_results) {
    $numrows = mysqli_num_rows($q_results);
    $results = array();
    if ($numrows > 0) {
      while ($row = mysqli_fetch_assoc($q_results)) { 
        $results[] = $row;
      } 
    }

    return $results;
  }

}